#include <libpq-fe.h>
#include <cstring>
#include <iostream>

using namespace std;

void Recup(PGconn *);
int main()
{
  int retour;
  char donnees_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=a.queru user=a.queru password=P@ssword";
  PGPing retour_ping;
  PGconn *connexion;
  retour_ping = PQping(donnees_connexion);

// QUESTION 3.1

  if(retour_ping == PQPING_OK)
  {
    cout << "Le serveur est accessible" << endl;
    connexion = PQconnectdb(donnees_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)
    {

//QUESTION 3.2 et 3.3

      cout << "La connexion au serveur de base de données '" << PQhost(connexion) << "' a été établie avec les paramètres suivants :" << endl;
      cout << "* Utilisateur : " << PQuser(connexion) << endl;
      cout << "* Mot de passe : " ;
      char  mdp = strlen(PQpass(connexion));

      for(int i = 0; i < mdp; i++)
      {
        cout << "*";
      }

// QUESTION 3.4 et 3.6
	
	void Recup(PGconn *connexion)
     {
  int nom, ligne, info, NombColonnes;
  PGresult *res = PQexec(connexion, "SET SCHEMA 'si6'; SELECT *	FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';");

         if(PQresultStatus(res) == PGRES_TUPLES_OK)
         {
         cout << "\tPGRES_TUPLES_OK" << endl; // <- Afficher si la requête fonctionne correctement
         NombColonnes = PQnfields(res);

// QUESTION 3.7

             for(nom = 0; nom < NombColonnes; nom++)
             {
             cout << " | " << PQfname(res, nom); // <- Sépare le noms des colonnes par un |
             }

     cout << "\n" << endl;

          for(ligne = 0; ligne < PQntuples(res); ligne++)
          {
             for(info = 0; info < NombColonnes; info++)
              {
              cout << "|" << PQgetvalue(res, ligne, info); 
              }

      cout << "\n" << endl;
    }

    else
    {
      cerr << " La connexion n'a pas pu être établie" << endl;
      retour = 1;
    }

  return retour;
}
